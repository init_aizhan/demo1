<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="wp-content/themes/init/css/theme.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    
    <header id="header">
    <div class="container">
<div class="header">
<a class='header__list header__list--active' href="">Home</a>
    <a class='header__list' href="">About me</a>
    <a class='header__list' href="">Skills</a>
    <a class='header__list' href="">Portfolio</a>
    <a class='header__list' href="">Contacts</a>
</div>  
<div class="header__adaptive">
<div class="burger">
		<span></span>
		<span></span>
		<span></span>
	</div>
</div> 

</header>



   <main>



</div>
<section id="my-card">
<div class="container">
<div class="my-card">
<div class="my-card__text">
<h1 class="my-card__text--name">Denis <br>
Novik</h1>
<div class="my-card__text--profession">UX | UI designer <br>
24 years old, Minsk</div>
<div class="my-card__text--lang"><span class="ru">RU</span> | ENG</div>
</div>
<div class="my-card__photo">
    <img src="wp-content/themes/init/css/images/photo_of_me.png" alt="">
</div>
<div class="my-card__photo-adaptive">
    <img src="wp-content/themes/init/css/images/my-photo_adaptive.png" alt="">
</div>
</div>
</div>
</section>


<section id="about-me">
    <div class="container">
        <div class="about-me">
        <h2 class="header-section">About me</h2>
    <p>Hi, I'm Denis – UX/UI designer from Minsk. I'm interested in design and everything connected with it.</p>
    <p>I'm studying at courses "Web and mobile design interfaces" in IT-Academy.</p>
    <p>Ready to implement excellent projects with wonderful people.</p>

        </div>

    </div>
  
</section>

<section id="skills">
    <div class="container">
        <div class="skills">
        <h2 class="header-section">Skills</h2>
        <div class="skills__intro">I work in such programs as</div>
        <div class="skills__list">
            <div class="skill">
<div class="skill__img">
<img src="wp-content/themes/init/css/images/ps.png" alt="">
</div>
<p>Adobe Photoshop</p>
<div class="stars">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-light.png" alt="">
</div>

            </div>

            <div class="skill">
<div class="skill__img">
<img src="wp-content/themes/init/css/images/ai.png" alt="">
</div>
<p>Adobe Illustrator</p>
<div class="stars">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-light.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-light.png" alt="">
</div>

            </div>
            <div class="skill">
<div class="skill__img">
<img src="wp-content/themes/init/css/images/ae.png" alt="">
</div>
<p>Adobe After Effects</p>
<div class="stars">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-light.png" alt="">
</div>

            </div>
            <div class="skill">
<div class="skill__img">
<img src="wp-content/themes/init/css/images/figma.png" alt="">
</div>
<p>Figma</p>
<div class="stars">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
    <img src="wp-content/themes/init/css/images/Star-dark.png" alt="">
</div>

            </div>
           
        </div>
    </div>

    </div>
</section>
<section id="portfolio">
    <div class="container">
        <div class="portfolio">
            <h2 class="header-section">Portfolio</h2>
            <div class="portfolio-items">
                <div class="portfolio-items__item">
                    <img src="wp-content/themes/init/css/images/portfolio-1.png" alt="">
                    <a href="">Online fashion store - Homepage</a>
                </div>
                <div class="portfolio-items__item">
                    <img src="wp-content/themes/init/css/images/portfolio-2.png" alt="">
                    <a href="">Reebok Store - Concept</a>
                </div>
                <div class="portfolio-items__item">
                    <img src="wp-content/themes/init/css/images/portfolio-3.png" alt="">
                    <a href="">Braun Landing Page - Concept</a>
                </div>
            </div>
        </div>
    </div>
</section>



</main> 
<footer id="footer">
    <div class="container">
<div class="footer">
    <h2 class="header-section">Contacts</h2>
    <div class="footer__items">
    <p>Want to know more or just chat? 
You are welcome!</p>
<button>Send message</button>
<div class="footer__items__icons">
    <img src="wp-content/themes/init/css/images/linkdin.png" alt="">
    <img src="wp-content/themes/init/css/images/inst.png" alt="">
    <img src="wp-content/themes/init/css/images/be.png" alt="">
    <img src="wp-content/themes/init/css/images/ball.png" alt="">
</div>
<div class="footer__items__links">
<p>Like me on</p>
<a href="">LinkedIn</a>, <a href="">Instagram</a>, <a href="">Behance</a>, <a href="">Dribble</a>
</div>
    </div>



</div>
    </div>

<section class="burger-menu-section close">
    <div class="container">
    <div class="burger-menu">

<div class="burger-menu__links">
<a href="">Home</a>
<a href="">About me</a>
<a href="">Skills</a>
<a href="">Portfolio</a>
<a href="">Contacts</a>
</div>

<div class="burger-menu__btns">

<div class="lang">RU | <span class="eng">ENG</span></div>
</div>

</div>
    </div>
</section>
</footer>
</div>

</div>
   <script src="wp-content/themes/init/js/burgerMenu.js" ></script>
</body>
</html>