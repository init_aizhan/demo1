const burgerMenuBtn = document.querySelector(".burger")
const burgerMenu = document.querySelector(".burger-menu-section")


burgerMenuBtn.addEventListener('click', () => {
    burgerMenu.classList.toggle('open')
    burgerMenuBtn.classList.toggle('show')
})

